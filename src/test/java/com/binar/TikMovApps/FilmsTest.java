package com.binar.TikMovApps;

import com.binar.TikMovApps.controller.FilmController;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class FilmsTest {

    @Autowired
    private FilmController filmController;

    @Test
    @DisplayName("1). Test Add Film")
    public void addFilm() {
        String filmAdded = filmController.addFilm("F564", "Tokyo Ghoul", "Sedangtayang");
        Assertions.assertEquals("Add Film Success!", filmAdded);
    }

    @Test
    @DisplayName("2). Test Update Film")
    public void updateFilm(){
        filmController.updateFilm("F433", "Ghoul", "Sedangtayang",41);
    }

    @Test
    @DisplayName("3). Test Delete Film")
    public void deleteFilm(){
        filmController.deleteFilm(35);
    }

    @Test
    @DisplayName("4). Test tampilkan film sedang tayang")
    public void allFilm(){
        filmController.allFilm();
    }

    @Test
    @DisplayName("5). Menampilkan jadwal dari film tertentu")
    public void getByNamaFilm(){
        filmController.getByNamaFilm("Ankora");
    }
}
