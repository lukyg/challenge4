package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Users;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public interface UserService {
    public void saveUser(String username, String password, String email);
    public void updateUser(String username, String email, String password, Integer userId);
    public void deleteUsers(Integer userId);
    public List<Users> userList();

}
