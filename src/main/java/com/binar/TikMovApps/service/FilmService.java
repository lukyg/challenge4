package com.binar.TikMovApps.service;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface FilmService {
    public void saveFilm(String filmCode, String namaFilm, String statusTayang);
    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId);
    public void deleteFilm(Integer filmId);
    public List<Films> filmsList();

}
