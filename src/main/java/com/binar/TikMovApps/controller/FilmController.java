package com.binar.TikMovApps.controller;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Schedules;
import com.binar.TikMovApps.service.FilmService;
import com.binar.TikMovApps.service.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class FilmController {

    @Autowired
    private FilmService filmService;

    @Autowired
    private ScheduleService scheduleService;

    public String addFilm(String filmCode, String namaFilm, String statusTayang){
        filmService.saveFilm(filmCode, namaFilm, statusTayang);
        return "Add Film Success!";
    }

    public void updateFilm(String filmCode, String namaFilm, String statusTayang, Integer filmId){
        filmService.updateFilm(filmCode, namaFilm, statusTayang, filmId);
    }

    public void deleteFilm(Integer filmId){
        filmService.deleteFilm(filmId);
    }

    public void allFilm(){
        List<Films> filmsList = filmService.filmsList();
        System.out.println("Film ID\t\tCode Film\t\tNama Film\t\tStatus");
        filmsList.forEach(films -> {
            if(films.getStatusTayang().equals("Sedangtayang")){
                System.out.println(films.getFilmId()+"\t\t\t"+films.getFilmCode()+"\t\t\t"+films.getNamaFilm()+
                        "\t\t\t"+films.getStatusTayang());
            }
        });
    }

    public List<Schedules> getByNamaFilm(String namaFilm) {
        List<Schedules> films = scheduleService.getByNamaFilm(namaFilm);
        System.out.println("Nama Film\t\tTanggal Tayang\t\tJam Mulai\t\tJam Selesai\t\tHarga Tiket");
        films.forEach(flm-> {
            System.out.println(flm.getFilmId().getNamaFilm()+"\t\t\t"+flm.getTanggalTayang()+"\t\t\t"+flm.getJamMulai()+
                    "\t\t\t"+flm.getJamSelesai()+"\t\t\tRp. "+flm.getHargaTiket());
        });
        return films;
    }
}
