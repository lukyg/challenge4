package com.binar.TikMovApps.controller;

import com.binar.TikMovApps.model.Films;
import com.binar.TikMovApps.model.Users;
import com.binar.TikMovApps.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;


@Controller
public class UsersController {

    @Autowired
    private UserService userService;

    public String addUser(String username, String password, String email){
        userService.saveUser(username, password, email);
        return "Add User Success!";
    }

    public void updateUser(String username, String email, String password, Integer userId) {
        userService.updateUser(username, email, password, userId);
    }

    public void deleteUser(Integer userId){
        userService.deleteUsers(userId);
    }

    public void allUsers(){
        List<Users> usersList = userService.userList();
        System.out.println("userId\tusername\t\temail\t\t\t\tpassword");
        usersList.forEach(users -> {
            System.out.println(users.getUserId()+"\t\t"+users.getUsername()+
                   "\t\t"+users.getEmail()+"\t"+users.getPassword());
        });
    }
}
