package com.binar.TikMovApps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TikMovApps {

    public static void main(String[] args) {
        SpringApplication.run(TikMovApps.class, args);
    }

}
