package com.binar.TikMovApps.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@Entity(name = "seats")
public class Seats {
    @EmbeddedId
    private SeatsId seatsId;
}
